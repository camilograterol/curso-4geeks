//Objetos en Javascript
// hay 2 formas de crear objetos

//var superman = {}
//var superman = new Object()

//las llaves me permiten definir las propiedades del objeto
//mientras que new Object() lo crea vacio
//normalmente vamos a utilizar las {}

var superman = {
    nombre: "superman",
    "nombre real": "Clark Kent",
    volar: function(){},
    lazer: function(){},
}
console.log(superman);
console.log(superman["nombre real"]);
console.log(superman.nombre);
console.log(superman.volar());

// saber si existe una propiedad en un objeto
console.log("edad" in superman);
console.log(superman.edad !== undefined)

// Saber todas las propiedades del objeto
for(var key in superman){
   console.log(key + ":" + superman[key]);
}

//añadir propiedades al objeto
superman.ciudad = "Metropolis";
console.log(superman);

// Objeto anidado o conjunto de objetos
justice_league = {
    superman: { realName: "Clark Kent"},
    batman: { realName: "Bruce Wayne"},
    wonderWoman: { realName: "Diana Prince"}
}

console.log(justice_league.superman.realName);



function saludo(options){
    options = options || {} ;
    saludo = options.saludo || "Hola";
    nombre = options.nombre || "visitante";
    edad = options.edad || 18;

    return saludo + "! Mi nombre es: " + nombre + " y tengo " + edad + "  años de edad";
}

console.log(saludo());



//datos = {nombre: "Camilo", edad: 21};
//console.log(mensaje(datos));
//console.log(mensaje({nombre:"Camilo", edad: 21}));



// --->Javascript Object Notation or JSON
var alumno = '{"nombre": "Pedro", "edad": 25, "Ciudad": "Caracas"}';
alumno = JSON.parse(alumno);
console.log(alumno.nombre);
console.log(alumno.edad);

var clark = JSON.stringify(superman);
console.log(clark);



// Objeto Math
console.log(Math.PI);
console.log(Math.LN2);

var sueldo = 1320.45;
console.log(Math.ceil(sueldo));
console.log(Math.floor(sueldo));
console.log(Math.round(sueldo));

console.log(Math.random());
console.log(Math.floor(Math.random() * 5) + 1);

console.log(Math.abs(-1345.456));

console.log(Math.pow(3,2));

// objetos Date();
var hoy = new Date();
console.log(hoy);

//devuelve el dia actual
console.log(hoy.getDay());

var dias = [ "dom", "lun", "mar", "mie", "jue", "vie", "sab"];
var meses = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"]
var fecha = new Date();

console.log(dias[fecha.getDay()]);
console.log(meses[fecha.getMonth()]);
console.log(fecha.getHours()+":"+fecha.getMinutes());

//Expresiones Regulares

//patrones
var pattern_1 = /\w+ing/;

prueba = pattern_1.test("testing");
console.log(prueba);

prueba2 = pattern_1.test("red");
console.log(prueba2);

//otra forma de ver un patron
var pattern_2 = new RegExp('\w+ing');

prueba3 = pattern_2.test("selling");
console.log(prueba3);

prueba4 = pattern_2.test("blue");
console.log(prueba4);

console.log(pattern_1.exec("joke"));
console.log(pattern_1.exec("joking"));

var soloVocales = /[aeiou]/;
console.log(soloVocales.test("r"));
console.log(soloVocales.test("ra"));
console.log(soloVocales.test("rA"));

var alfabeto = /[A-Z]/;
var numeros = /[0-9]/;
var alfabetoSinMayus = /[^A-Z]/; // 

// cuando veamos esto dentro de la exp reg 
// equivale a lo siguiente

// \w = [A-Za-z0-9_] coincide con cualquier caracter que sea una palabra
// \W = [^A-Za-z0-9_] coincide con cualquier caracter que no sea una palabra
// \d = [0-9] coincide con cualquier caracter que sea un digito
// \D = [^0-9] coincide con cualquier caracter que no sea un digito
// \s = [\t\r\n\f] coinciden con cualquier caracter que sea de espacio en blanco
// \S = [^\t\r\n\f] coincide con cualquiuer caracter que no sea espacio en blanco



// Propiedades de las Expresiones Regulares


// /[a-z]/g    La propiedad global hace que el patrón devuelva todas las coincidencias. 
            //Por defecto, el patrón solo busca la primera aparición de una coincidencia.


// /[a-z]/i   La propiedad ignoreCase hace que el patrón no distinga entre mayúsculas y minúsculas. 
            //Por defecto, ellos son sensibles a las mayúsculas


// /[a-z]/m   La propiedad de líneas múltiples hace que el patrón sea multilínea. 
            //Por defecto, un patrón para al final de una línea.






