// Comentarios

// Comentario corto

/*
Comentario largo o Multiples Lineas

kuyfitdotf
lhvpyvpuvypiyvpiv
khvhipuuiubiuhoihoioh

*/

// Gramatica de Javascript

a = "Hola Mundo"
alert(a)

// o puedo hacerlo

a = "Hola Mundo";
alert(a);

// Bloques de Codigo

//function() seguido de
{
    // Este bloque contiene dos sentencias (los bloques de codigo siempre empiezan con una funcion)
    var a = "Hola";
    alert(a);

}

// Tipos de datos
/*
string
number
Boolean
undefined
null
*/

let nombre = 'pedro'; typeof nombre; //string
let edad = 5; typeof edad; //number
let soltero = true; typeof soltero; //boolean
typeof apellido; // undefined
let flag = null; typeof flag; //object
let estudiante = { nombre: 'Camilo', apellido: 'Graterol'}; typeof estudiante; //object

// Escapes de Caracteres
let apellido = "D\'ambrosio"; //de esta manera no toma la comilla simple (') dentro de la interpretacion

let mensaje = "Esto es un backslash \\"; 
// El primer backslash escapa el primero, lo que evita que el backslash salte las comillas
// para un salto de lidea ponemos \n 

let mensaje = "cita: \" esto es una cita \" " ; // de esta manera \" escapa las dobles comillas
let mensaje2 = "Esta es mi primera linea \n y esta es mi segunda linea"; //\n nueva linea
let mensaje3 = "Esto es otra prueba \r"; // \r Retorno de carga
let mensaje4 = "Esto esta separado por \t tabulacion"; // \t tabulado 

// reglas para definir una variable

$nombre = "camilo"; //puedo comenzarlas con $

_apellido = "graterol"; // puedo comenzar con _

segundonombre = "Antoine"; // Puedo definirla solo letras

tercerNombre = "Antonio"; // Puedo definirla usando camelCase

cuarto_nombre = "no se"; // puedo separarla con _

nombre5 = "quinto"; // puedo incluir numero pero no comenzar con numeros

// Todas las variables son "case sensitive" , NOMBRE no es igual a Nombre o NomBre

// Palabras reservadas

/*
abstract, boolean, break, byte , case, catch, char, class, const,

continue, debugger, default, delete, do, double, else, enum, export, extends, false,

final, finally, float, for, function, goto, if, implements, import, in instaceof,

int, interface, long, negative, new, null, pakage, private, protected, public, return,

short, static, super, switch, synchrinized, this, throw, trhows, transient, true, try, 

typeof, var, volatile, void, while, with

*/

//asignations
var a ="";
var a, b, c;
var a=1, b, c=3;
b=7;