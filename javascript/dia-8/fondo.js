// Capturando el elemento Button por su ID
var button = document.getElementById('cambiarFondo');
// Definiendo Arreglo con colores
let colors = ["red", "orange", "yellow", "green", "blue", "indigo", "violet"];

function cambiarFondo(){
    document.body.style.background = colors[Math.floor(7*Math.random())];
}

button.addEventListener('click', cambiarFondo);