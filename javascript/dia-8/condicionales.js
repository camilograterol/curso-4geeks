// if(condition){

// }

var a = 5;
var b = 3;
var c = 8;

// (a!=b) (a > b b != 0) (a > b || b != 0) tambien pueden ir dentro del if por ejemplo

if(a==b){ 

    //sentencia a ejecutar

}

// else funciona como decir "si no"
if(a>b){
    console.log("a es mayor que b")
} else{
    console.log("b es mayor que a")
}

// nested if
if(a > b && a > c){
    console.log("A es el mayor")
}else if(b > a && b > c){
    console.log("B es el mayor")
}else{
    console.log("Ces el mayor")
}

var edad = 13;
if(edad < 18){  
    console.log("Lo siento no puede ver este contenido")
}

// Operador Ternario 
n = 5;
//condicion      IF      Verdadero      Else        Falso
n%2===0          ? console.log("Es par") : console.log("Es impar")

flag = true;
flag ? alert("Verdadero") : alert("falso");

edad = 21;
edad >= 18 ? console.log ("es mayor") : console.log("es menor");

// sentencia switch
var opt = 4;

if(opt === 1){
    console.log("Opcion del menu 1");
}else if(opt === 2){
    console.log("Opcion del menu 2");
}else if(opt === 3){
    console.log("Opcion del menu 3");
}else if(opt === 4){
    console.log("opcion del menu 4");
}else if(opt === 5){
    console.log("Opcion del menu 5")
}else{
    console.log("Opcion del menu incorrecta");
}

switch(opt){
    case 1:
        console.log("Opcion del menu 1");
        break;
    case 2:
        console.log("Opcion del menu 2");
        break;
    case 3:
        console.log("Opcion del menu 3");
        break;
    case 4:
        console.log("Opcion del menu 4");
        break;
    case 5:
        console.log("Opcion del menu 5");
        break;
    default:
        console.log("Opcion del menu incorrecta");
        break;
}

