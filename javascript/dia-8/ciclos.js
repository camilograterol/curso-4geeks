// Ciclos While, do while, for, for anidados

// while es igual a if pero 
//solo se va a ejecutar siempre que la condicion sea verdadera

var n = 1;
while(n <= 10){
    console.log(n);
    n++;
} // "Se va a manterner ejecutando mientras la consicion sea verdadera" -prof



// do while
//el do while se va a ejecutar al menos una vez
//sin importar que la condicion dentro del while sea verdad o no
n = 1;
do {
    console.log(n);
    n++;
}while(n<=10); // "Se va a mantener ehecutando mientras la condicion sea verdaddera,
               // y se ejecutara solo una vez."


// Ciclo for (inicializador; condicion; incremento)
for(var i = 1; i <= 10; i++){
    console.log(i);
}

var multi = [[1, 1], [10, 10], [20, 20]];

for(var i = 0; i < multi.length; i++){
    for(var j=0; j < multi [i].lenght; j++){
        console.log(multi[i][j]);
    }
}