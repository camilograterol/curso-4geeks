function saludo(){
    console.log("hola mundo desde la funcion");
}

saludo();

var saludo2 = function(){
    console.log("Invocando una funcion (saludo2)")
}

saludo2();

function mayor(){
    var edad = 18;
    if(edad < 18){
        return "Es menor";
    }else{
        return "Es mayor";
    }
}

console.log(mayor());

function mayor2(){

    var edad = 18;
    if(edad < 18){
        return false;
    }else{
        return true;
    }
}

 if(mayor2()){
     console.log("Es mayor");
 }else{
     console.log("Es menor");
 }

 function sumar(a, b){
     return a + b;
 }

 console.log(sumar(40, 23));

 //definimos counter como una variable fuera de la funcion
 //si la definimos adentro no la podemos llamar en otra funcion
 var counter = 0;

 function contar(){
     var suma = 0;
    return counter+=1;
 }
 //console.log(suma);
 console.log(contar());



 //uso del forEach

 var arr = [12,32,45,54];
 arr.forEach(function(valor, posicion){
     console.log("Nota: " + posicion + " - " + valor);
 });

 function square(valor){
     return valor * valor;
 }

 arr.map(function (valor, index){
     console.log(valor*valor);
 });

 arr.reduce(function(anterior, siguiente){
     return anterior + siguiente;
 })
 console.log(total);

 numeros = arr.filter(function(numero){
     return numero%2===0;
 });
 console.log(numeros);

 var nombres = ["Luis", "Pedro", "Miguel", "Juan"];
 nombre = nombres.filter(function(nombre){
     if(nombre.indexOf("j") != -1){
         return nombre;
     }
 });

 console.log(nombre);