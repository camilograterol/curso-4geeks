var quiz = [
    ["Cual es el nombre real de Superman", "Clark Kent"],
    ["Cual es el nombre real de Spiderman", "Peter Parker"],
    ["Cual es el nombre real de Batman", "Bruce Wayne"],
    ["Cual es el nombre real de Wonder Woman", "Diana Prince"],
];

var score = 0;

var $pregunta = document.getElementById("pregunta");
var $score = document.getElementById("score");
var $feedback = document.getElementById("feedback");
var $start = document.getElementById
function update(elemento, contenido, estilo){
    var p = elemento.firstChild || document.createElement("p");
    p.textContent = contenido;
    elemento.appendChild(p);
    if(estilo){
        p.className = estilo;
    }
}

play(quiz);



function play(quiz) {
    for (var i = 0, pregunta, respuesta, max = quiz.length; i < max; i++) {
        pregunta = quiz[i][0];
        respuesta = preguntar(pregunta);
        verificar(respuesta);
    }
    gameOver();


    function preguntar(pregunta){
        update($pregunta, quiz.pregunta + pregunta);
        return prompt("Ingrese Su Respuesta");
    }

    function verificar(respuesta){
        if (respuesta === quiz[i][1]){
            //alert("Respuesta Correcta");
            update($feedback, "Respuesta Correcta", "bien");
            score += 10;
            update($score, score);
        } else {
            update($feedback, "Respuesta Incorrecta", "mal");
            score -= 5;
            update($score, score);
        }

    }

    function gameOver() {
        //alert("Game Over, Su puntuacion fue " + score + " puntos");
        update($pregunta, "Game Over, Su puntuacion fue " + score + " puntos");
    }
}
