var quiz = [
    ["Cual es el nombre real de Superman", "Clark Kent"],
    ["Cual es el nombre real de Spiderman", "Peter Parker"],
    ["Cual es el nombre real de Batman", "Bruce Wayne"],
    ["Cual es el nombre real de Wonder Woman", "Diana Prince"],
];

var score = 0;

for(var i = 0, max=quiz.length; i < max; i++){
    var respuesta = prompt(quiz[i][0]);
    if(respuesta === quiz[i][1]){
        alert("Respuesta Correcta");
        score += 10;
    }else{
        alert("Respuesta Incorrecta");
        score -= 5;
    }
}

alert("Game Over, Su puntuacion fue " + score + " puntos");