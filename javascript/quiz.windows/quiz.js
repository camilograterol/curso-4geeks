function update(elemento, contenido, estilo) {
    var p = elemento.firstChild || document.createElement("p");
    p.textContent = contenido;
    elemento.appendChild(p);
    if (estilo) {
        p.className = estilo;
    }
}

var quiz = {
    "nombre": "Quiz de Nombres de Super Heroes",
    "descripcion": "Cuantos nombres de heroes conoces?",
    "pregunta": "Cual es el nombre real de: ",
    "preguntas": [
        { "pregunta": "Superman", "respuesta": "Clark Kent" },
        { "pregunta": "Spiderman", "respuesta": "Peter Parker" },
        { "pregunta": "Batman", "respuesta": "Bruce Wayne" },
        { "pregunta": "Wonderwoman", "respuesta": "Diana Prince" },
    ],
}

var score = 0;

var $pregunta = document.getElementById("pregunta");
var $score = document.getElementById("score");
var $feedback = document.getElementById("feedback");
var $start = document.getElementById("start");
var $form = document.getElementById("respuesta");
var $timer = document.getElementById("timer");

$start.addEventListener("click", function () { play(quiz) }, false);
ocultar($form);

function play(quiz) {
    var time = 20;
    update($timer, time);
    var interval = window.setInterval(cuentaAtras, 1000);
    ocultar($start);
    mostrar($form);
    $form.addEventListener("submit", function(event){
        event.preventDefault();
        verificar($form[0].value);
    }, false);

    var i = 0;
    elegirPregunta();

    function preguntar(pregunta) {
        $form[0].value = "";
        $form[0].focus();
        update($pregunta, quiz.pregunta + pregunta);
    }
    function verificar(respuesta) {
        if (respuesta === quiz.preguntas[i].respuesta) {
            //alert("Respuesta Correcta");
            update($feedback, "Respuesta Correcta", "bien");
            score += 10;
            update($score, score);
        } else {
            //alert("Respuesta Incorrecta");
            update($feedback, "Respuesta Incorrecta", "mal");
            score -= 5;
            update($score, score);
        }
        i++;
        if(i=== quiz.preguntas.length){
            gameOver();
        }else {
            elegirPregunta();
        }
    }
    function gameOver() {
        //alert("Game Over, Su puntuacion fue " + score + " puntos");
        window.clearInterval(interval);
        update($pregunta, "Game Over, Su puntuacion fue " + score + " puntos");
        ocultar($form);
        mostrar($start);
        score = 0;
        update($score, score);
    }
    function elegirPregunta(){
        pregunta = quiz.preguntas[i].pregunta;
        preguntar(pregunta);
    }
    function cuentaAtras(){
        time--;
        update($timer, time);
        if(time <= 0){
            gameOver();
        }
    }
}

function ocultar(elemento){
    elemento.style.display = "none";
}
function mostrar (elemento){
    elemento.style.display = "block";
}
