var quiz = [
    ["Cual es el nombre real de Superman", "Clark Kent"],
    ["Cual es el nombre real de Spiderman", "Peter Parker"],
    ["Cual es el nombre real de Batman", "Bruce Wayne"],
    ["Cual es el nombre real de Wonder Woman", "Diana Prince"],
];

var score = 0;

play(quiz);

function play(quiz) {
    for (var i = 0, pregunta, respuesta, max = quiz.length; i < max; i++) {
        pregunta = quiz[i][0];
        respuesta = preguntar(pregunta);
        verificar(respuesta);
    }
    gameOver();


    function preguntar(preguntar){
        return prompt(pregunta);
    }

    function verificar(respuesta){
        if (respuesta === quiz[i][1]){
            alert("Respuesta Correcta");
            score += 10;
        } else {
            alert("Respuesta Incorrecta");
            score -= 5;
        }

    }

    function gameOver() {
        alert("Game Over, Su puntuacion fue " + score + " puntos");
    }
}
