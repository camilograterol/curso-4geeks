var $square = document.getElementById("square");
var angulo = 0;


/*
setInterval(function(){
    angulo = (angulo + 5)%360;
    $square.style.transform = "rotate("+ angulo +"deg)";

}, 1000/6);
*/

function rotar(){
    angulo = (angulo + 5)%360;
    $square.style.transform = "rotate("+ angulo +"deg)";
    id= window.requestAnimationFrame(rotar);
}

document.getElementById("start").addEventListener("click", function(){
    id = window.requestAnimationFrame(rotar);
    console.log(id);
});

document.getElementById("stop").addEventListener("click", function(){
    window.cancelAnimationFrame(id);
});
